﻿using System.Collections.Generic;
using Assets.Scripts.Terrain;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor {
    [CustomEditor(typeof(RoadElement))]
    public class RoadEditor : UnityEditor.Editor {

        private static readonly float HANDLE_SIZE = .5f;
        
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
        }

        private RoadElement.RoadElementIntersection selected;
        private bool isSelected;

        public virtual void OnSceneGUI() {
            GUIStyle skin = new GUIStyle(GUI.skin.textArea);
            RoadElement element = (RoadElement)target;
            if (!isSelected) {
                foreach (RoadElement.RoadElementIntersection elementConnectionOption in element.intersections) {
                    Vector3 position = elementConnectionOption.transform.position;

                    if (Handles.Button(position, Quaternion.identity, HANDLE_SIZE, HANDLE_SIZE, Handles.CircleHandleCap)) {
                        selected = elementConnectionOption;
                        isSelected = true;
                    }

                }
            } else if(selected != null){
                float offset = 0;
                GameObject roadConfigObj = Resources.Load<GameObject>("road-config");
                RoadConfig roadConfig = roadConfigObj.GetComponent<RoadConfig>();
                List<RoadElement> possible = roadConfig.GetElements(selected.myPoint);
                foreach (RoadElement optionElement in possible) {
                    string connectionName = optionElement.gameObject.name;
                    Handles.color = Color.black;
                    Handles.Label(selected.transform.position + Vector3.up * offset + (Vector3.right * HANDLE_SIZE * .5f), connectionName, skin);
                    if (Handles.Button(selected.transform.position + Vector3.up*offset + (Vector3.down*HANDLE_SIZE*.15f),Quaternion.identity, HANDLE_SIZE*.5f, HANDLE_SIZE*.5f, Handles.CubeHandleCap)) {
                        //GameObject prefab = connectionName.Equals(element.elementName) ? (GameObject)PrefabUtility.GetPrefabParent(optionElement.gameObject) : optionElement.gameObject;
                        GameObject spawnedObj = PrefabUtility.InstantiatePrefab(optionElement.gameObject) as GameObject;
                        if (spawnedObj != null) {
                            RoadElement spawned = spawnedObj.GetComponent<RoadElement>();
                            spawned.transform.parent = selected.transform;
                            spawned.transform.localPosition = Vector3.zero;
                            spawned.transform.localScale = Vector3.one;
                            foreach (RoadElement.RoadElementIntersection spawnedIntersection in spawned.intersections) {
                                if (spawnedIntersection.myPoint == roadConfig.GetOpposite(selected.myPoint)) {
                                    spawned.transform.position -= (spawnedIntersection.transform.position - spawned.transform.position);
                                    break;
                                }
                            }
                            spawned.transform.parent = optionElement.transform.parent;
                        }
                        isSelected = false;
                        selected = null;
                        Selection.activeGameObject = spawnedObj;
                        break;
                    }
                    offset += (HANDLE_SIZE * .5f) + .02f;
                }
            }
        }
    }
}

