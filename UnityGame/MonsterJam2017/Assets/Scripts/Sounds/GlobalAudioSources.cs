﻿using System;
using UnityEngine;

namespace Assets.Scripts.Sounds {
    public class GlobalAudioSources : MonoBehaviour {

        public AudioSource bgm;
        public AudioSource sfx;

        private static GlobalAudioSources instance;

        public void Awake() {
            instance = this;
        }
        public void Start() {
            
        }


        public static AudioSource GetBgm() {
            return instance.bgm;
        }

        internal static AudioSource GetSfx() {
            return instance.sfx;
        }
    }
}
