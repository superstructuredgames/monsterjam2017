﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.CharacterMotion;
using Assets.Scripts.Eating;
using UnityEngine;

namespace Assets.Scripts.Player {

    [Serializable]
    public class PlayerInputConfig {
        public KeyCode up;
        public KeyCode down;
        public KeyCode left;
        public KeyCode right;
        public KeyCode eat;
    }

    public class PlayerInput {
        private readonly CharacterMotor motor;
        private readonly EaterCharacter eater;
        private readonly Dictionary<KeyCode, MotionDirection> inputDirections;
        private readonly LinkedList<KeyCode> directionStack;
        private readonly KeyCode eatKey;

        private MotionDirection lastDirection;
        

        public PlayerInput(PlayerInputConfig config, CharacterMotor motor, EaterCharacter eater) {
            this.motor = motor;
            this.eater = eater;
            directionStack = new LinkedList<KeyCode>();
            lastDirection = MotionDirection.None;
            inputDirections = new Dictionary<KeyCode, MotionDirection> {
                {config.up, MotionDirection.Up },
                {config.down, MotionDirection.Down },
                {config.left, MotionDirection.Left },
                {config.right, MotionDirection.Right }
            };
            eatKey = config.eat;
        }
        
        public void Update () {
            foreach (KeyValuePair<KeyCode, MotionDirection> pair in inputDirections) {
                if(Input.GetKeyDown(pair.Key)){
                    directionStack.AddFirst(pair.Key);
                }
                if (Input.GetKeyUp(pair.Key)) {
                    directionStack.Remove(pair.Key);
                }
            }
            MotionDirection currentDirection = MotionDirection.None;
            if (directionStack.Any()) {
                currentDirection = inputDirections[directionStack.First.Value];
            }
            if (currentDirection != lastDirection) {
                lastDirection = currentDirection;
                motor.RequestMotion(currentDirection);
            }
            if (Input.GetKeyUp(eatKey)) {
                eater.Eat();
            }
            
        }
    }
}
