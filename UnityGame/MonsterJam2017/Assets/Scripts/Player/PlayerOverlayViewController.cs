﻿using System.Globalization;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.Player {
    public class PlayerOverlayViewController : MonoBehaviour {

        public float topBarFactor  = 1;
        public float bottomBarFactor = 5;
        public float viewCorruption = 0.0f;
        public float viewHunger = 1;

        private PlayerCharacter player;
        private MarkedUiView view;
        
        public void Start() {
            player = FindObjectOfType<PlayerCharacter>();
        }

        public void OnViewLoad(MarkedUiView aView) {
            view = aView;
        }

        public void Update() {
            if (player != null && view != null) {
                float displayHunger = Mathf.Lerp(viewHunger, player.GetCurrentFoodPercent(), Time.deltaTime * bottomBarFactor);
                float displayCorruption = Mathf.Lerp(viewCorruption, player.GetCurrentCorruptionPercent(), Time.deltaTime * topBarFactor);
                viewHunger = displayHunger;
                viewCorruption = displayCorruption;
                view.SetAttribute("id=dual-bar", "top-fill-percent", displayCorruption.ToString(CultureInfo.InvariantCulture));
                view.SetAttribute("id=dual-bar", "bot-fill-percent", displayHunger.ToString(CultureInfo.InvariantCulture));
                view.UpdateLayout("id=dual-bar");
                /*UpdateBar("hunger-bar", player.GetCurrentFoodPercent(), ref viewHunger);
                UpdateBar("evil-bar", player.GetCurrentCorruptionPercent(), ref viewCorruption);*/
            }
        }

        private void UpdateBar(string barId, float value, ref float oldValue) {
            float displayValue = Mathf.Lerp(oldValue, value, Time.deltaTime*topBarFactor);
            oldValue = displayValue;
            view.SetAttribute("id=" + barId, "for-fill-percent", displayValue.ToString(CultureInfo.InvariantCulture));
            view.UpdateLayout("id=" + barId);
        }
    }
}
