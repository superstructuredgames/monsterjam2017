﻿using System;
using Assets.Scripts.Animations;
using Assets.Scripts.CharacterMotion;
using Assets.Scripts.Corruption;
using Assets.Scripts.Eating;
using Assets.Scripts.Hunger;
using Assets.Scripts.Objectives;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.Player {
    public class PlayerCharacter : MonoBehaviour {

        public event Action OnStarved;
        public event Action OnVictory;

        public Transform toMove;
        public AnimationTriggerer anim;
        public AnimationEventHandler animEventHandler;
        public MotionBoundingConfig boundingConfig;
        public MotionAnimation[] motionAnimTriggers;
        public PlayerInputConfig inputConfig;
        public HungerConfig hungerConfig;
        public EaterConfig eaterConfig;
        public CorruptionConfig corruptionConfig;
        public ObjectiveReacherConfig objectiveConfig;

        private CharacterMotor motor;
        private MotionAnimator motorAnim;
        private PlayerInput input;
        private EaterCharacter eater;
        private HungerCharacter hungerCharacter;
        private CorruptionCharacter corruption;
        private ObjectiveReacher objReacher;
        
        public void Awake() {
            motor = new CharacterMotor(toMove, boundingConfig);
            hungerCharacter = new HungerCharacter(motor, anim, hungerConfig);
            motorAnim = new MotionAnimator(motionAnimTriggers, anim, motor, boundingConfig.walkSource, boundingConfig.walkClip);
            eater = new EaterCharacter(toMove, eaterConfig, anim, animEventHandler);
            input = new PlayerInput(inputConfig, motor, eater);
            corruption = new CorruptionCharacter(corruptionConfig);
            objReacher = new ObjectiveReacher(toMove, anim, objectiveConfig);
            hungerCharacter.Start();
            eater.OnEat += HandleEat;
            eater.OnEatFinished += HandleEatFinished;
            objReacher.OnObjectiveReached += HandleObjectiveReached;
            hungerCharacter.OnStarved += HandleStarved;
        }

        private void HandleStarved() {
            Debug.Log("Failure!");
            DisableActions();
            // ReSharper disable once UseNullPropagation
            if (OnStarved != null) {
                OnStarved.Invoke();
            }
        }

        public void DisableActions() {
            motor.Disable();
            eater.Disable();
        }

        public void EnableActions(){
            motor.Enable();
            eater.Enable();
        }

        private void HandleObjectiveReached() {
            Debug.Log("Victory!");
            DisableActions();
            // ReSharper disable once UseNullPropagation
            if (OnVictory != null) {
                OnVictory.Invoke();
            }
        }

        private void HandleEatFinished() {
            motor.Enable();
        }

        private void HandleEat(Edible eaten) {
            motor.Disable();
            hungerCharacter.Process(eaten.GetEatenRoot());
            corruption.ProcessCorruption(eaten.GetEatenRoot());
        }

        public void Update() {
            input.Update();
            motor.Update();
            motorAnim.Update();
            hungerCharacter.SetMaxFoodPercent(1.0f - corruption.GetCurrentCorruptionPercent());
            hungerCharacter.Update();
            objReacher.Update();
            eater.Update();
        }

        public void OnDrawGizmos() {
            CharacterMotor.OnDrawGizmos(boundingConfig, toMove);
        }

        public float GetCurrentFoodPercent() {
            return hungerCharacter.GetCurrentFoodPercent();
        }

        public float GetCurrentCorruptionPercent() {
            return corruption.GetCurrentCorruptionPercent();
        }
    }
}
