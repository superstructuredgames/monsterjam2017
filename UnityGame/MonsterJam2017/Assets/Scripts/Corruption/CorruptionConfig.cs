﻿using System;

namespace Assets.Scripts.Corruption {
    [Serializable] public class CorruptionConfig {
        public float defaultCorruption = 0;
        public float maxCorruption = 100;
    }
}
