﻿using UnityEngine;

namespace Assets.Scripts.Corruption {
    public class CorruptionIncrementer : MonoBehaviour, CorruptionProvider {

        public float increment;

        public float UpdatCorruption(float current) {
            return current + increment;
        }
    }
}
