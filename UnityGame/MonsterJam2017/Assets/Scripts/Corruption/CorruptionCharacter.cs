﻿using UnityEngine;

namespace Assets.Scripts.Corruption {
    public class CorruptionCharacter {

        private readonly CorruptionConfig config;

        private float currentCorruption;
        
        public CorruptionCharacter(CorruptionConfig config) {
            this.config = config;
            currentCorruption = config.defaultCorruption;
        }

        public void ProcessCorruption(GameObject rootProvider) {
            CorruptionProvider[] providers = rootProvider.GetComponentsInChildren<CorruptionProvider>();
            foreach (CorruptionProvider corruptionProvider in providers) {
                currentCorruption = corruptionProvider.UpdatCorruption(currentCorruption);
                currentCorruption = currentCorruption < 0 ? 0 : currentCorruption;
                currentCorruption = currentCorruption > config.maxCorruption ? config.maxCorruption : currentCorruption;
            }
        }

        public float GetCurrentCorruptionPercent() {
            return currentCorruption/config.maxCorruption;
        }
        
    }
}
