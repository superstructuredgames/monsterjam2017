﻿using UnityEngine;

namespace Assets.Scripts.Cameras
{
    public class CameraPanPoint : MonoBehaviour {
        public string panPointName;
        public float size;
        
        public static CameraPanPoint Find(string panPointName) {
            CameraPanPoint[] points = FindObjectsOfType<CameraPanPoint>();
            CameraPanPoint found = null;
            foreach (CameraPanPoint cameraPanPoint in points) {
                if (cameraPanPoint.panPointName.Equals(panPointName)) {
                    found = cameraPanPoint;
                    break;
                }
            }
            return found;
        }
    }
}
