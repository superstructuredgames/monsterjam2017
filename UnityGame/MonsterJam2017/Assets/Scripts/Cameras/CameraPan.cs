﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Cameras
{
    [Serializable]
    public class CameraPanConfig {
        public string startPanPointName;
        public string endPanPointName;
        public AnimationCurve panCurve;
        public bool enableBoundOnFinish = true;
        public bool disableOnOrientStart = true;
    }

    public class CameraPan {
        private readonly CameraPanConfig config;

        public CameraPan(CameraPanConfig config) {
            this.config = config;
        }

        public void Run(Action onFinished) {
            CameraPanPoint start = CameraPanPoint.Find(config.startPanPointName);
            CameraPanPoint end = CameraPanPoint.Find(config.endPanPointName);
            end.StartCoroutine(RunPan(start, end, onFinished));
        }

        public void OrientStart() {
            CameraPanPoint start = CameraPanPoint.Find(config.startPanPointName);
            if (start != null) {
                Camera.main.transform.position = start.transform.position;
                Camera.main.orthographicSize = start.size;
                BoundedCameraFollow.Disable();
            }
        }

        private IEnumerator<YieldInstruction> RunPan(CameraPanPoint start, CameraPanPoint end, Action onFinished) {
            BoundedCameraFollow.Disable();
            Vector3 startPos = start == null ? Camera.main.transform.position : start.transform.position;
            Vector3 endPos = end.transform.position;
            float startSize = start == null ? Camera.main.orthographicSize : start.size;
            float endSize = end.size;
            int len = config.panCurve.length;
            Keyframe last = config.panCurve[len - 1];
            float duration = last.time;
            float startTime = Time.time;
            while (Time.time - startTime <= duration) {
                float eval = config.panCurve.Evaluate(Time.time - startTime);
                Camera.main.orthographicSize = Mathf.Lerp(startSize, endSize, eval);
                Camera.main.transform.position = Vector3.Lerp(startPos, endPos, eval);
                yield return new WaitForEndOfFrame();
            }
            Camera.main.orthographicSize = endSize;
            Camera.main.transform.position = endPos;
            if (config.enableBoundOnFinish) {
                BoundedCameraFollow.Enable();
            }
            // ReSharper disable once UseNullPropagation
            if (onFinished != null) {
                onFinished.Invoke();
            }
        }
    }
}
