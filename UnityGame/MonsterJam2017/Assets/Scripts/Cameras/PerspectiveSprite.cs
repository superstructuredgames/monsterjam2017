﻿using UnityEngine;

namespace Assets.Scripts.Cameras {


    [ExecuteInEditMode] public class PerspectiveSprite : MonoBehaviour {
        public Transform root;


        public void Start() {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f);
        }

        public void Update() {
            SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sprite in sprites) {
                Vector3 position = (root == null) ? sprite.bounds.min : root.position;
                sprite.sortingOrder = (int)Camera.main.WorldToScreenPoint(position).y * -1;
            }
        }
    }
}
