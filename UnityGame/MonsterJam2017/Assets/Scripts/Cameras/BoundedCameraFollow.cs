﻿using UnityEngine;

namespace Assets.Scripts.Cameras {
    public class BoundedCameraFollow : MonoBehaviour {

        public BoxCollider2D followCollider;
        public string followTag;
        private Transform toFollow;
        private static BoundedCameraFollow instance;
        public bool isDisabled;

        public void Awake() {
            toFollow = GameObject.FindGameObjectWithTag(followTag).transform;
            instance = this;
        }

        public void OnPreRender() {
            if (isDisabled) return;

            Vector3 checkPosition = new Vector3(toFollow.position.x, toFollow.position.y, transform.position.z);
            Bounds viewBounds = followCollider.bounds;
            if (!viewBounds.Contains(checkPosition)) {
                Vector3 point = viewBounds.ClosestPoint(checkPosition);
                Vector3 direction = checkPosition - point;
                transform.position = transform.position + direction;
            }
        }
        
        public static void Disable() {
            instance.isDisabled = true;
        }

        public static void Enable() {
            instance.isDisabled = false;
        }
    }
}
