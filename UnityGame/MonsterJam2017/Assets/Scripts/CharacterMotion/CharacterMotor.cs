﻿using System;
using UnityEngine;

namespace Assets.Scripts.CharacterMotion {
    public class CharacterMotor {

        public event Action OnEnable;

        private readonly Transform toMove;
        private readonly MotionBoundingConfig boundingConfig;

        private static readonly MotionDirection[] SIDES = { MotionDirection.Up, MotionDirection.Down, MotionDirection.Left, MotionDirection.Right };
        private static readonly Vector3[] RAY_BUFFER = new Vector3[2];
        private static readonly RaycastHit2D[] HIT_BUFFER = new RaycastHit2D[100];
        private MotionDirection requestedDirection;
        private MotionDirection lastDirection;
        private Vector3 lastMovement;
        private bool isEnabled = true;

        public CharacterMotor(Transform toMove, MotionBoundingConfig boundingConfig) {
            this.toMove = toMove;
            this.boundingConfig = boundingConfig;
        }

        public void RequestMotion(MotionDirection direction) {
            requestedDirection = direction;
        }

        public MotionDirection GetLastMotion() {
            return lastDirection;
        }
        
        public void Update () {
            
            if (!isEnabled) return;
            lastDirection = requestedDirection;
            Vector3 dir = MotionUtil.ToVector(requestedDirection);
            dir = dir*(boundingConfig.speed*Time.deltaTime);
            foreach (MotionDirection side in SIDES) {
                for (int i = 0; i < boundingConfig.sideCheckCount; i++) {
                    GetCheckRay(side, boundingConfig, toMove, RAY_BUFFER, i);
                    Vector3 direction = (RAY_BUFFER[1] - RAY_BUFFER[0]);
                    int hitCount = Physics2D.RaycastNonAlloc(RAY_BUFFER[0], direction, HIT_BUFFER, direction.magnitude, boundingConfig.layer);
                    if (hitCount > 0){
                        Vector3 sideDir = MotionUtil.ToVector(side);
                        dir.x = Math.Sign(dir.x) == Math.Sign(sideDir.x) ? 0 : dir.x;
                        dir.y = Math.Sign(dir.y) == Math.Sign(sideDir.y) ? 0 : dir.y;
                        break;
                    }
                }
            }
            lastMovement = dir;
            toMove.position = toMove.position + dir;
        }

        public static void OnDrawGizmos(MotionBoundingConfig config, Transform toMove) {
            Gizmos.color = Color.green;
            foreach (MotionDirection side in SIDES) {
                for (int i = 0; i < config.sideCheckCount; i++) {
                    GetCheckRay(side, config, toMove, RAY_BUFFER, i);
                    Gizmos.DrawLine(RAY_BUFFER[0], RAY_BUFFER[1]);
                }
            }
        }

        private static void GetCheckRay(MotionDirection side, MotionBoundingConfig config, Transform toMove, Vector3[] output, float index) {
            Vector3 sideDir = MotionUtil.ToVector(side);
            Vector3 origin = toMove.position;
            Vector3 direction = Vector3.zero;
            GetCheckValue(ref origin, ref direction, sideDir.x, config, index, Vector3.right, Vector3.up);
            GetCheckValue(ref origin, ref direction, sideDir.y, config, index, Vector3.up, Vector3.right);
            output[0] = origin;
            output[1] = origin + direction;
        }

        private static void GetCheckValue(ref Vector3 origin, ref Vector3 direction, float sideVal, MotionBoundingConfig config, float index, Vector3 dir, Vector3 opDir) {
            if (Mathf.Abs(sideVal) > .001f){
                origin += dir * (config.offset * Mathf.Sign(sideVal));
                direction = dir * (config.checkDistance * Mathf.Sign(sideVal));
                float increment = (config.offset * 2 + config.checkDistance * 2) / config.sideCheckCount;
                origin += opDir * (increment * index - config.offset - config.checkDistance + (.5f * increment));
            }
        }


        public Vector3 GetLastMovement() {
            return isEnabled ? lastMovement : Vector3.zero;
        }

        public void Disable() {
            isEnabled = false;
            if (boundingConfig.walkSource.isPlaying && boundingConfig.walkSource.clip == boundingConfig.walkClip) {
                boundingConfig.walkSource.Stop();
            }
        }

        public void Enable() {
            isEnabled = true;
            // ReSharper disable once UseNullPropagation
            if (OnEnable != null) {
                OnEnable.Invoke();
            }
        }
    }
}
