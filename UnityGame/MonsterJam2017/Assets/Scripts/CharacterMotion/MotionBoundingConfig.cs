﻿using System;
using UnityEngine;

namespace Assets.Scripts.CharacterMotion {
    [Serializable] public class MotionBoundingConfig {
        public float speed = 5;
        public float offset;
        public float checkDistance;
        public LayerMask layer;
        public int sideCheckCount;
        public AudioSource walkSource;
        public AudioClip walkClip;
    }
}
