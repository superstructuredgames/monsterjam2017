﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Animations;
using UnityEngine;

namespace Assets.Scripts.CharacterMotion {

    [Serializable]
    public class MotionAnimation {
        public string triggerName;
        public MotionDirection direction;
        
    }

    public class MotionAnimator {

        private readonly AnimationTriggerer anim;
        private readonly CharacterMotor motor;
        private readonly AudioSource source;
        private readonly AudioClip walkClip;
        private readonly Dictionary<MotionDirection, MotionAnimation> motionMap = new Dictionary<MotionDirection, MotionAnimation>();

        private MotionDirection lastDirection;
        private bool isFiredThisFrame;

        public MotionAnimator(MotionAnimation[] triggerBindings, AnimationTriggerer anim, CharacterMotor motor, AudioSource source, AudioClip walkClip) {
            this.anim = anim;
            this.motor = motor;
            this.source = source;
            this.walkClip = walkClip;
            foreach (MotionAnimation motionAnimation in triggerBindings) {
                motionMap[motionAnimation.direction] = motionAnimation;
            }
            motor.OnEnable += HandleEnable;
        }

        private void HandleEnable() {
            FireCurrentAnimation();
        }

        private void FireCurrentAnimation() {
            MotionDirection current = motor.GetLastMotion();
            lastDirection = current;
            string triggerName = motionMap[lastDirection].triggerName;
            anim.Trigger(triggerName);
            isFiredThisFrame = true;
            if (lastDirection != MotionDirection.None){
                source.clip = walkClip;
                source.loop = true;
                source.Play();
            }
            else if (lastDirection == MotionDirection.None && source.clip == walkClip){
                source.Stop();
            }
        }

        public void Update() {
            MotionDirection current = motor.GetLastMotion();
            if (current != lastDirection && !isFiredThisFrame) {
                FireCurrentAnimation();
            }
            isFiredThisFrame = false;
        }
    }
}
