﻿using UnityEngine;

namespace Assets.Scripts.Hunger{
    public class HungerIncrementer : MonoBehaviour, HungerModifier {

        public float increment;

        public float GetUpdatedFoodCount(float initial) {
            return initial + increment;
        }
    }
}
