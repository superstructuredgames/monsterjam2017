﻿using System;
using UnityEngine;

namespace Assets.Scripts.Hunger {
    [Serializable] public class HungerConfig {
        public float maxFood;
        public float distanceToFood = 1.0f;
        public string starveAnimTrigger = "die";
        public AudioSource audioSource;
        public AudioClip starveClip;
    }
}
