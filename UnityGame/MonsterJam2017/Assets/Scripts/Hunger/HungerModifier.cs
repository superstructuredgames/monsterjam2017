﻿namespace Assets.Scripts.Hunger {
    interface HungerModifier {
        float GetUpdatedFoodCount(float initial);
    }
}
