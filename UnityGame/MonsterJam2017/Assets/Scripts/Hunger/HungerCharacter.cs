﻿using System;
using Assets.Scripts.Animations;
using Assets.Scripts.CharacterMotion;
using UnityEngine;

namespace Assets.Scripts.Hunger {
    public class HungerCharacter{
        private readonly CharacterMotor motor;
        private readonly AnimationTriggerer anim;
        private readonly HungerConfig config;
        private float currentFood;
        private float maxFoodPercent;
        private bool isStarved;

        public event Action OnStarved;

        public HungerCharacter(CharacterMotor motor, AnimationTriggerer anim, HungerConfig config) {
            this.motor = motor;
            this.anim = anim;
            this.config = config;
        }

        public void Start() {
            maxFoodPercent = 1;
            currentFood = GetMaxFood();
        }

        public void Update() {
            Vector3 movement =motor.GetLastMovement();
            float moveDist = movement.magnitude;
            float consumed = Mathf.Abs(moveDist*config.distanceToFood);
            float maxFood = GetMaxFood();
            currentFood = currentFood > maxFood ? maxFood : currentFood;
            currentFood = currentFood - consumed;
            currentFood = currentFood < 0 ? 0 : currentFood;
            if (!isStarved && GetCurrentFoodPercent() <= 0.0f){
                isStarved = true;
                anim.Trigger(config.starveAnimTrigger);
                config.audioSource.clip = config.starveClip;
                config.audioSource.loop = false;
                config.audioSource.Play();
                // ReSharper disable once UseNullPropagation
                if (OnStarved != null) {
                    OnStarved.Invoke();
                }
            }
        }

        public float GetCurrentFoodPercent() {
            return currentFood / config.maxFood;
        }

        public void SetMaxFoodPercent(float percent) {
            maxFoodPercent = percent;
            maxFoodPercent = maxFoodPercent > 1 ? 1 : maxFoodPercent;
            maxFoodPercent = maxFoodPercent < 0 ? 0 : maxFoodPercent;
        }

        public float GetMaxFoodPercent() {
            return maxFoodPercent;
        }

        public void Process(GameObject toProcess) {
            HungerModifier[] modifiers = toProcess.GetComponentsInChildren<HungerModifier>();
            float maxFood = GetMaxFood();
            foreach (HungerModifier hungerModifier in modifiers) {
                currentFood = hungerModifier.GetUpdatedFoodCount(currentFood);
                currentFood = currentFood > maxFood ? maxFood : currentFood;
                currentFood = currentFood < 0 ? 0 : currentFood;
            }
        }

        private float GetMaxFood() {
            return config.maxFood*maxFoodPercent;
        }
    }
}
