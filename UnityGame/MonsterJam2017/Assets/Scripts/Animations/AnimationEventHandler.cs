﻿using System;
using UnityEngine;

namespace Assets.Scripts.Animations {
    public class AnimationEventHandler : MonoBehaviour {

        public event Action<string> OnAnimEvent;

        public void HandleEvent(string s) {
            // ReSharper disable once UseNullPropagation
            if (OnAnimEvent != null) {
                OnAnimEvent.Invoke(s);
            }
        }
    }
}
