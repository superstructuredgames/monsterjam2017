﻿using UnityEngine;

namespace Assets.Scripts.Animations {
    public class AnimationTriggerer : MonoBehaviour {

        public Animator anim;


        public void Start() {
            if (anim == null) {
                anim = GetComponent<Animator>();
            }
        }
        public void Trigger(string triggerName) {
            anim.SetTrigger(triggerName);
        }
    }
}
