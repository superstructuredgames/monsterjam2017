﻿using System.ComponentModel;
using Assets.Scripts.Player;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.GameFlow {
    public class VictoryTextController : MonoBehaviour{

        [TextArea] public string happyText;
        [TextArea] public string sadText;
        private MarkedUiView view;
        

        public void OnViewLoad(MarkedUiView aView) {
            view = aView;
        }

        public void Start() {
            string text = happyText;
            if (FindObjectOfType<PlayerCharacter>().GetCurrentCorruptionPercent() > 0.0f) {
                text = sadText;
            }
            view.SetAttribute("id=description", "text-value", text);
            view.UpdateLayout("id=description");
        }
    }
}
