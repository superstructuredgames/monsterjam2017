﻿using System.Runtime.InteropServices;
using Assets.Scripts.Cameras;
using Assets.Scripts.Player;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.GameFlow {

    public class OverlayController : MonoBehaviour{
        public CameraPanConfig victoryPanConfig;
        public CameraPanConfig failPanConfig;

        private MarkedUiView view;
        private CameraPan failPan;
        private CameraPan victoryPan;
        
        public void OnViewLoad(MarkedUiView aView){
            view = aView;
        }

        public void Start() {
            victoryPan = new CameraPan(victoryPanConfig);
            failPan = new CameraPan(failPanConfig);
            PlayerCharacter player = FindObjectOfType<PlayerCharacter>();
            player.OnStarved += HandleStarved;
            player.OnVictory += HandleVictory;
        }

        private void HandleVictory() {
            view.SwitchWithTransition("victory", "swipe-up-delayed", false, null);
            victoryPan.Run(null);

        }

        private void HandleStarved() {
            view.SwitchWithTransition("fail", "swipe-up-delayed", false, null);
            failPan.Run(null);

        }
    }
}
