﻿using Assets.Scripts.Cameras;
using Assets.Scripts.Player;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.GameFlow {
    public class IntroFlowController : MonoBehaviour {
        public KeyCode startKey;
        private bool isStarted;
        private MarkedUiView view;
        public CameraPanConfig startPan;
        private CameraPan pan;

        public void OnViewLoad(MarkedUiView aView){
            view = aView;
        }

        public void Start() {
            FindObjectOfType<PlayerCharacter>().DisableActions();
            pan = new CameraPan(startPan);
            pan.OrientStart();
            Cursor.visible = false;
        }

        public void Update() {
            if (!isStarted && Input.GetKeyUp(startKey)) {
                isStarted = true;
                view.SwitchWithTransition("overlay", "introduction", false, null);
                pan.Run(() => {
                    FindObjectOfType<PlayerCharacter>().EnableActions();
                });                
            }
        }
    }
}
