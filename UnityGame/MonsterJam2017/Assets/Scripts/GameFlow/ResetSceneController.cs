﻿using Assets.Scripts.Player;
using Assets.Scripts.Sounds;
using MarkedUi;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.GameFlow {
    public class ResetSceneController : MonoBehaviour {

        public AudioClip bgmClip;
        public KeyCode resetKey = KeyCode.Space;
        public float delay = 2.0f;

        private bool isReloaded;
        private float startTime;

        public void OnViewLoad(MarkedUiView aView) {
            startTime = Time.time;  
        }

        public void Start() {
            GlobalAudioSources.GetBgm().clip = bgmClip;
            GlobalAudioSources.GetBgm().Play();
        }
        public void Update() {
            if (!isReloaded && Input.GetKeyUp(resetKey) && (Time.time - startTime > delay)) {
                isReloaded = true;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
