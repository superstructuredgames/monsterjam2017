﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Animations;
using Assets.Scripts.Cameras;
using UnityEngine;

namespace Assets.Scripts.Objectives {

    [Serializable]
    public class ObjectiveReacherConfig {
        public float reachDistance = 0.1f;
        public string yayAnimTrigger = "winning";
    }

    public class ObjectiveReacher {
        private readonly Transform root;
        private readonly AnimationTriggerer anim;
        private readonly ObjectiveReacherConfig config;
        

        private bool isReached;
        

        public event Action OnObjectiveReached;


        public ObjectiveReacher(Transform root, AnimationTriggerer anim, ObjectiveReacherConfig config) {
            this.root = root;
            this.anim = anim;
            this.config = config;
        }

        public void Update() {
            if (!isReached) {
                Vector3 direction = ObjectivePoint.GetPosition() - root.position;
                if (direction.magnitude <= config.reachDistance) {
                    isReached = true;
                    anim.Trigger(config.yayAnimTrigger);
                    // ReSharper disable once UseNullPropagation
                    if (OnObjectiveReached != null) {
                        OnObjectiveReached.Invoke();
                    }
                }
            }
        }

        public void Reset() {
            isReached = false;
        }
    }
}
