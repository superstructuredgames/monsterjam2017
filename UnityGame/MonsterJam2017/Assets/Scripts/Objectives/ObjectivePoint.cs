﻿using UnityEngine;

namespace Assets.Scripts.Objectives {
    public class ObjectivePoint : MonoBehaviour {
        
        private static ObjectivePoint instance;
        

        public void Start() {
            if (instance == null) {
                instance = this;
            }
            
        }

        public void OnDestroy() {
            if (instance == this) {
                instance = null;
            }
        }

        public static Vector3 GetPosition() {
            return instance.transform.position;
        }
        
    }
}
