﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Objectives {

    public class ObjectiveIndicator  : MonoBehaviour {

        public float worldToMeter = 1.0f;
        public Text text;
        public RectTransform toRotate;
        public string playerTag;
        private Transform player;
        
        public void Start() {
            player = GameObject.FindGameObjectWithTag(playerTag).transform;
        }

        public void Update() {
            if (player != null) {
                Vector3 position = toRotate.position;
                Vector3 followPositionRaw = ObjectivePoint.GetPosition();
                Vector3 followPosition = Camera.main.WorldToScreenPoint(followPositionRaw);
                Vector3 playerPosition = player.position;
                playerPosition.z = 0;
                position.z = 0;
                followPositionRaw.z = 0;
                followPosition.z = 0;   
                Vector3 direction = (followPosition - position);
                toRotate.right = -(direction);
                text.transform.up = Vector3.up;
                float meters = (followPositionRaw - playerPosition).magnitude * worldToMeter;
                text.text = string.Format("{0:#} m", meters);
            }
        }

    }
}
