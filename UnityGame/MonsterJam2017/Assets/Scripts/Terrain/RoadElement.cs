﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Terrain {
    public class RoadElement : MonoBehaviour{

        [Serializable]
        public enum RoadIntersectionId {
            Top, Bottom, Left, Right
        }

        [Serializable]
        public class RoadElementIntersection {
            public Transform transform;
            public RoadIntersectionId myPoint;
        }

        public List<RoadElementIntersection> intersections;
        

    }
}
