﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Terrain {
    public class RoadConfig  : MonoBehaviour {

        

        public RoadElement[] elements;

        private static readonly Dictionary<RoadElement.RoadIntersectionId, RoadElement.RoadIntersectionId> OPPOSITE_MAP = new Dictionary<RoadElement.RoadIntersectionId, RoadElement.RoadIntersectionId>() {
            {RoadElement.RoadIntersectionId.Bottom, RoadElement.RoadIntersectionId.Top},
            {RoadElement.RoadIntersectionId.Top, RoadElement.RoadIntersectionId.Bottom},
            {RoadElement.RoadIntersectionId.Left, RoadElement.RoadIntersectionId.Right},
            {RoadElement.RoadIntersectionId.Right, RoadElement.RoadIntersectionId.Left}
        };

        public RoadElement.RoadIntersectionId GetOpposite(RoadElement.RoadIntersectionId id) {
            return OPPOSITE_MAP[id];
        }

        public List<RoadElement> GetElements(RoadElement.RoadIntersectionId intersection) {
            List<RoadElement> matchElements = new List<RoadElement>();
            RoadElement.RoadIntersectionId opposite = OPPOSITE_MAP[intersection];
            foreach (RoadElement allElement in elements) {
                foreach (RoadElement.RoadElementIntersection allIntersection in allElement.intersections) {
                    if (allIntersection.myPoint == opposite) {
                        matchElements.Add(allElement);
                        break;
                    }
                }
            }
            return matchElements;
        }
    }
}
