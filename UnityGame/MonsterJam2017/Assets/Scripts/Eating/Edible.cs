﻿using UnityEngine;

namespace Assets.Scripts.Eating {
    public interface Edible {
        void HandleConsumed();
        GameObject GetEatenRoot();
    }
}
