﻿using System;
using UnityEngine;

namespace Assets.Scripts.Eating {
    public class TestEdible : MonoBehaviour, Edible {

        public GameObject root;

        public void HandleConsumed() {
            Debug.Log("I Am Eaten!!!");
            Destroy(root);
        }

        public GameObject GetEatenRoot() {
            return root;
        }
        
    }
}
