﻿using Assets.Scripts.Animations;
using Assets.Scripts.Sounds;
using UnityEngine;

namespace Assets.Scripts.Eating {

    public class AnimatedEdible : MonoBehaviour, Edible {

        public AnimationTriggerer anim;
        public GameObject toSpawnPrefab;
        public Transform spawnParent;
        public GameObject root;
        public string animTrigger = "eaten";
        public AudioClip eatSound;

        public void HandleConsumed() {
            GameObject spawned = Instantiate(toSpawnPrefab);
            spawned.transform.parent = spawnParent;
            spawned.transform.localPosition = Vector3.zero;
            spawned.transform.localRotation = Quaternion.identity;
            spawned.transform.localScale = Vector3.one;
            Destroy(gameObject);
            anim.Trigger(animTrigger);
            GlobalAudioSources.GetSfx().clip = eatSound;
            GlobalAudioSources.GetSfx().Play();
        }

        public GameObject GetEatenRoot() {
            return root;
        }
    }
}
