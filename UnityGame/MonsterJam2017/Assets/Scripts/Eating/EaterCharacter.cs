﻿using System;
using Assets.Scripts.Animations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Eating {

    public class EaterCharacter {

        public event Action<Edible> OnEat;
        public event Action OnEatFinished;

        private readonly Transform root;
        private readonly EaterConfig config;
        private readonly AnimationTriggerer anim;
        private readonly GameObject eatIndicator;
        private bool isEating;
        private bool isDisabled;
        
        private Edible nearest;

        public EaterCharacter(Transform root, EaterConfig config, AnimationTriggerer anim, AnimationEventHandler animEventHandler) {
            this.root = root;
            this.config = config;
            this.anim = anim;
            animEventHandler.OnAnimEvent += HandleAnimEvent;
            eatIndicator = Object.Instantiate(config.indicationPrefab);
            eatIndicator.name = "eat-indicator";
            HideIndicator();
        }

        private void HideIndicator() {
            eatIndicator.SetActive(false);
            eatIndicator.transform.parent = root;
            eatIndicator.transform.localPosition = Vector3.zero;
            eatIndicator.transform.localRotation = Quaternion.identity;
        }

        private void HandleAnimEvent(string eventName) {
            if (eventName.Equals(config.eatFinishedEvent, StringComparison.CurrentCultureIgnoreCase)) {
                // ReSharper disable once UseNullPropagation
                if (OnEatFinished != null) {
                    OnEatFinished.Invoke();
                }
                isEating = false;
            }
        }

        public void Update() {
            if (isDisabled) return;
            nearest = GetNearestEdible();
            if (nearest == null && eatIndicator.activeInHierarchy) {
                HideIndicator();
            }
            if (nearest != null && !eatIndicator.activeInHierarchy) {
                ShowIndicator();
            }
            if (nearest != null) {
                eatIndicator.transform.position = nearest.GetEatenRoot().transform.position + config.indicationOffset;
            }
        }

        private void ShowIndicator() {
            eatIndicator.SetActive(true);
        }

        public void Eat() {
            if (isDisabled) return;
            if (!isEating && nearest != null) {
                isEating = true;
                anim.Trigger(config.eatTrigger);
                config.eatSource.clip = config.eatClip;
                config.eatSource.PlayDelayed(0.25f);
                config.eatSource.loop = false;
                // ReSharper disable once UseNullPropagation
                if (OnEat != null) {
                    OnEat.Invoke(nearest);
                }
                nearest.HandleConsumed();
            }
        }

        private Edible GetNearestEdible() {
            EaterCollider collider = config.collider;
            int ediblesCount = collider.GetPossibleEdibleCount();
            float nearestDistance = float.MaxValue;
            Edible found = null;
            for (int i = 0; i < ediblesCount; i++) {
                Edible edible = collider.GetPossibleEdible(i);
                float distance = (edible.GetEatenRoot().transform.position - root.position).sqrMagnitude;
                if (distance < nearestDistance) {
                    found = edible;
                    nearestDistance = distance;
                }
            }
            return found;
        }

        public void Disable() {
            if (config.eatSource.isPlaying && config.eatSource.clip == config.eatClip) {
                config.eatSource.Stop();
            }
            HideIndicator();
            isDisabled = true;
        }

        public void Enable() {
            isDisabled = false;
        }
    }
}
