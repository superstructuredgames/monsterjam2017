﻿using System;
using UnityEngine;

namespace Assets.Scripts.Eating {
    [Serializable] public class EaterConfig {

        public EaterCollider collider;
        public string eatTrigger;
        public string eatFinishedEvent;
        public GameObject indicationPrefab;
        public Vector3 indicationOffset;
        public AudioSource eatSource;
        public AudioClip eatClip;
    }
}
