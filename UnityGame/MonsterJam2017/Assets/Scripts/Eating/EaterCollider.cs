﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Eating {
    public class EaterCollider : MonoBehaviour {

        private readonly LinkedList<Edible> possibleEdibles = new LinkedList<Edible>();

        public void OnTriggerEnter2D(Collider2D other) {
            Edible[] edibles = other.GetComponents<Edible>();
            if (edibles.Length > 0) {
                foreach (Edible edible in edibles) {
                    possibleEdibles.AddLast(edible);
                }
            }
        }

        public void OnTriggerExit2D(Collider2D other) {
            Edible[] edibles = other.GetComponents<Edible>();
            if (edibles.Length > 0) {
                foreach (Edible edible in edibles){
                    if (possibleEdibles.Contains(edible)) {
                        possibleEdibles.Remove(edible);
                    }
                    
                }
            }
        }

        public int GetPossibleEdibleCount() {
            return possibleEdibles.Count;
        }

        public Edible GetPossibleEdible(int index) {
            return possibleEdibles.ElementAt(index);
        }
    }
}
